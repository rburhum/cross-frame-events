# cross-frame-events


## Getting started
```python
pip install -r requirements.txt
flask run
```

## description
One page has two child iframes (map_frame and dashboard_frame). The parent frame uses jquery to 
bind to the click event of the button in one child frame (map_frame) after it has loaded. 
During the click event, we inspect the selected option in the drop down and change the url
of the second frame (dashboard_frame) dashboard to include the query parameters. I found
the query parameters using the Metabase docs.