from app import app
from flask import render_template

@app.route("/")
def index():
    return render_template("./index.html")

@app.route("/frame1.html")
def frame1():
    return render_template("./frame1.html")